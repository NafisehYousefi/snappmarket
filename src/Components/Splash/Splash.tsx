import styled from 'styled-components';

interface SplashProps {
    showMap: () => void;
}

export default function Splash(props: SplashProps) {
    const { showMap } = props;
    
    const SplashContainer = styled.div`
    padding: 18px;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    `;

    const SplashImage = styled.div`
    background:red;
    width:80px;
    height:80px;
    border-radius:50px;
    margin: 0 auto;
    `;

    const Context = styled.p`
    color:#ADD8E6;
    font-weight: bold;
    `;

    const handleShowMap = () => {
        setTimeout(function () { showMap() }, 3000);
    }

    return (
        <SplashContainer>
            <SplashImage />
            <Context>Awesome Map</Context>
            {handleShowMap()}
        </SplashContainer>
    )
}