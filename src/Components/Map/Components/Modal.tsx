import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useForm, SubmitHandler } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';

interface IFormInput {
  title: string;
  description: string;
}

export default function Modal() {

  const { register, handleSubmit, formState: { errors }, setValue } = useForm<IFormInput>();

  const {
    showModal,
    lastPosition,
    markers,
    showEditModal
  } = useSelector((state: Reducer.CombinedReducer) => {
    return {
      showModal: state.modalReducer.showModal,
      lastPosition: state.markersReducer.lastPosition,
      markers: state.modalReducer.markers,
      showEditModal: state.modalReducer.showEditModal
    }
  }
  )
  const dispatch = useDispatch();

  useEffect(() => {

    if (!!showEditModal) {
      setValue('title', showEditModal.Title)
      setValue('description', showEditModal.Description)
    }

  }, [])

  const Modal = styled.div` {
    z-index:10000;
    position: fixed;
    top: 0;
    left: 0;
    width:100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.6);
    ${(showModal) => !!showModal ? `diaplay: block;` : 'diaplay: none;'}
  }`;

  const FormContainer = styled.section` {
    position:fixed;
    background: white;
    width: 80%;
    height: auto;
    top:50%;
    left:50%;
    transform: translate(-50%,-50%);
  }`;

  const Form = styled.form`
    padding: 18px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    `;

  const Input = styled.input`
    display: block;
    box-sizing: border-box;
    width: 100%;
    outline: 0;
    border-width: 0 0 2px;
    border-color: blue;
    padding: 10px 15px;
    margin-bottom: 10px;
    font-size: 14px;
    `;

  const Label = styled.label`
    line-height: 2;
    text-align: left;
    display: block;
    margin-bottom: 13px;
    margin-top: 20px;
    font-size: 14px;
    color: gray;
    font-weight: bold;
    `;

  const Submit = styled.input`
    background: blue;
    color: white;
    text-transform: uppercase;
    border: none;
    margin: 40px 10px 0 0;
    padding: 10px;
    font-size: 14px;
    font-weight: 100;
    letter-spacing: 10px;
    border-radius: 4px;
    `;

  const Button = styled.button`
    background: gray;
    color: white;
    text-transform: uppercase;
    border: none;
    margin-top: 40px;
    padding: 10px;
    font-size: 14px;
    font-weight: 100;
    letter-spacing: 10px;
    border-radius: 4px;
    `;


  const onSubmit: SubmitHandler<IFormInput> = data => {
    if (!!showEditModal && markers.length) {
      let markerIndex = markers.findIndex(item => item.MarkerID === showEditModal.MarkerID);
      markers.splice(markerIndex, 1, { ...markers[markerIndex], Title: data.title, Description: data.description });

    }
    else {
      let marker: MarkerModel.Marker =
      {
        MarkerID: `${(lastPosition as any[])[0]},${(lastPosition as any[])[1]}`,
        Title: data.title,
        Description: data.description,
        Position: lastPosition
      }
      markers.push(marker);
    }

    dispatch({ type: 'SETALLPOSITION', payload: markers });
    dispatch({ type: 'SHOWMODAL', payload: false });

  }

  const handleCancelModal= () => {
    dispatch({ type: 'SHOWMODAL', payload: false });
    dispatch({
      type: 'SHOWEDITMODAL',
      payload: null
    });
  }

  return (
    <Modal>
      <FormContainer>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Label>Title</Label>
          <Input {...register('title', { maxLength: 20 })} /><br />
          <Label>Description</Label>
          <Input {...register('description')} /><br />
          <Submit type='submit' />
          <Button type='button' onClick={handleCancelModal}>
            Cancel
          </Button>
        </Form>
      </FormContainer>
    </Modal>

  )
}