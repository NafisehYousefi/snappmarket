import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import { useState, FormEvent } from 'react';

export default function Search() {

  const [searchInputValue, setSearchInputValue] = useState<string>('');

  const {
    markers,
    showResultSearch,
  } = useSelector((state: Reducer.CombinedReducer) => {
    return {
      markers: state.modalReducer.markers,
      showResultSearch: state.searchReducer.showResultSearch,
    }
  });

  const dispatch = useDispatch();

  const SearchComponent = styled.div`
    position: fixed;
    top: 20px;
    right: 20px;
    z-index: 9999;
    width: 80%;
  `;

  const Input = styled.input`
    display: block;
    box-sizing: border-box;
    width: 100%;
    border-radius: 4px;
    border: 1px solid #cac5c5;
    padding: 10px 15px;
    font-size: 14px;
    text-align: right;
    &:focus {
      outline: none;
  }

  `;

  const ItemContainer = styled.div`
    border-bottom-left-radius: 4px;
    border-bottom-right-radius: 4px;
    min-height: 40px;
    background-color: white;
    text-align: right;
    padding: 2px 14px;
  `;

  const MarkersList = styled.ul`
    list-style : none;
  `;

  const Marker = styled.li`
    padding: 4px;
    font-size: 12px;
  `;

  const goToMarker = (marker: MarkerModel.Marker) => {

    dispatch({
      type: 'SETLASTPOSITION',
      payload: marker.Position,
    });

    dispatch({ type: 'SHOWRESULTSEARCH', payload: false });
  }

  const handleSearchResult = () => {
    let markersFined = markers.filter(item => item.Title.toLowerCase().includes(searchInputValue.toLowerCase()));

    if (markersFined.length) {
      return markersFined.map((item, index) => {
        return (
          <Marker
            key={index}
            style={{ padding: '4px', fontSize: '12px', cursor: 'pointer' }}
            onClick={() => goToMarker(item)}
          >
            {item.Title}
          </Marker>
        )
      })
    }
    else {
      return <Marker style={{ padding: '4px', fontSize: '12px', cursor: 'pointer' }}>Not found</Marker>
    }

  }

  const handleSearchMarker = (e: FormEvent<HTMLInputElement>) => {
    setSearchInputValue(e.currentTarget.value);

    if (e.currentTarget.value) {
      dispatch({ type: 'SHOWRESULTSEARCH', payload: true });
    }
    else {
      dispatch({ type: 'SHOWRESULTSEARCH', payload: false });
    }

  }

  return (
    <SearchComponent>
      <Input type='text' placeholder='search' autoFocus={true} onChange={handleSearchMarker} value={searchInputValue} />
      {showResultSearch &&
        <ItemContainer>
          < MarkersList >
            {handleSearchResult()}
          </MarkersList>
        </ItemContainer>
      }
    </SearchComponent >
  )
}
