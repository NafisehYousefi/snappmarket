import { useSelector, useDispatch } from 'react-redux';
import 'leaflet/dist/leaflet.css';
import { Marker, useMapEvents, Tooltip } from 'react-leaflet';
import L from 'leaflet';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';

let DefaultIcon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow
});

interface MarkersProps {
  showModal: () => void,
}

export default function Markers(props: MarkersProps) {
  
  const {
    markers,
    lastPosition
  } = useSelector((state: Reducer.CombinedReducer) => {
    return {
      markers: state.modalReducer.markers,
      lastPosition: state.markersReducer.lastPosition
    }
  });

  const dispatch = useDispatch();

  const map = useMapEvents({
    click(e) {

      dispatch({
        type: 'SETLASTPOSITION',
        payload: e.latlng
      });

      map.flyTo(e.latlng, map.getZoom());
      props.showModal();
    }
  });
  
  map.flyTo(lastPosition, map.getZoom()); 

  const showMarkers = () => {

    if (markers.length) {
      return markers.map((positionMarker: MarkerModel.Marker) => {
        return (
          <Marker position={positionMarker.Position} icon={DefaultIcon}
            eventHandlers={{
              click: function (e) {
                dispatch({
                  type: 'SHOWEDITMODAL',
                  payload: positionMarker
                });

                dispatch({
                  type: 'SHOWMODAL',
                  payload: true
                });
              },
            }}
          >
            <Tooltip>
              {positionMarker.Title}
            </Tooltip>
          </Marker>
        )
      }

      )
    }
  }

  return markers === null ? null : <>{showMarkers()}</>
}
