declare namespace MarkerModel {
  export interface Marker {
    MarkerID: string
    Title: string
    Description: string
    Position: any
  }
}
