import 'leaflet/dist/leaflet.css';
import { useSelector, useDispatch } from 'react-redux';
import { MapContainer, TileLayer } from 'react-leaflet';
import Markers from './Components/Markers';
import Modal from './Components/Modal';
import Search from './Components/Search';

export default function Map() {

    const showModal = useSelector((state: Reducer.CombinedReducer) => state.mapReducer.showModal)
    const dispatch = useDispatch();

    const handleShowModal = () =>{
        dispatch({ type: 'SHOWMODAL', payload: true })
    }
    
    return (
        <div>
            {!!showModal && <Modal />}
            <Search />
            <MapContainer center={[51.505, -0.09]} zoom={13} scrollWheelZoom={false}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                />
                <Markers showModal={handleShowModal} />
            </MapContainer>
        </div>
    )
}