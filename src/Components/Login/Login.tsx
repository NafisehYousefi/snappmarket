import { useForm, SubmitHandler } from 'react-hook-form';
import styled from 'styled-components';

interface IFormInput {
    userName: string;
    password: string;
}

interface LoginProps {
    showSplash : (value:boolean)=>void;
}

export default function Login(props:LoginProps) {

    const {showSplash} = props;

    const Form = styled.form`
    background: #0e101c;
    padding: 18px;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius:8px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    `;

    const Error = styled.p`
    color: #bf1650;
    `;

    const Input = styled.input`
    display: block;
    box-sizing: border-box;
    width: 100%;
    border-radius: 4px;
    border: 1px solid white;
    padding: 10px 15px;
    margin-bottom: 10px;
    font-size: 14px;
    `;

    const Label = styled.label`
    line-height: 2;
    text-align: left;
    display: block;
    margin-bottom: 13px;
    margin-top: 20px;
    color: white;
    font-size: 14px;
    font-weight: 200;
    `;

    const Button = styled.input`
    background: #ec5990;
    color: white;
    text-transform: uppercase;
    border: none;
    margin-top: 40px;
    padding: 10px;
    font-size: 14px;
    font-weight: 100;
    letter-spacing: 10px;
    width: 100%;
    border-radius: 4px;
    `;


    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm<IFormInput>();

    const onSubmit: SubmitHandler<IFormInput> = data => {
        if(data.userName === 'test' && data.password === '12345'){
            showSplash(true);
        }
    }

    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <Label>User Name</Label>
            <Input {...register('userName', { required: true })} />
            {errors?.userName?.type === 'required' && <Error>This field is required</Error>}
            <Label>Password</Label>
            <Input type='password' {...register('password', { required: true })} />
            {errors?.password?.type === 'required' && <Error>This field is required</Error>}
            <Button type='submit' value='Login' data-testid='login' />
        </Form>
    );
}

