import Login from './Components/Login/Login';
import Splash from './Components/Splash/Splash';
import Map from './Components/Map/Map';
import { useSelector, useDispatch } from 'react-redux';

function App() {
  const {
    showSplash,
    showMap,
  } = useSelector((state: Reducer.CombinedReducer) => {
    return {
      showSplash: state.appReducer.showSplash,
      showMap: state.appReducer.showMap,
    }
  });

  const dispatch = useDispatch();

  const handleShowSplash = (splashStatus: boolean) => {
    if (splashStatus) {
      dispatch({ type: 'SHOWSPLASH' })
    }
  };

  const handleShowMap = () => {
    dispatch({ type: 'SHOWMAP' })
  };

  const handleShowComponents = () => {
    if (!showSplash && !showMap) {
      return <Login showSplash={handleShowSplash} />
    }
    else if (showSplash && !showMap) {
      return <Splash showMap={handleShowMap} />
    }
    else if (showMap) {
      return <Map />
    }
  }

  return (
    <div>
      {handleShowComponents()}
    </div>
  );
}

export default App;
