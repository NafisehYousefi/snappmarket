import React from 'react';
import { render } from '@testing-library/react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './Store/rootReducer';
import Login from './Components/Login/Login';

const store = createStore(rootReducer);

test('render appComponent', () => {
  render(
    <Provider store={store}>
      <Login showSplash={() => { }} />
    </Provider>
  )
});
