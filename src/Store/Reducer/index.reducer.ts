import { combineReducers } from 'redux';
import mapReducer from './map.reducer';
import markersReducer from './markers.reducer';
import modalReducer from './modal.reducer'
import searchReducer from './search.reducer'
import appReducer from './app.reducer'
export default combineReducers({
    mapReducer,
    markersReducer,
    modalReducer,
    searchReducer,
    appReducer
});