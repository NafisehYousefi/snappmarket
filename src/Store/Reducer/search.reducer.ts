import ActionsModel from './reducersModel/actionModel';
import {
  SETLASTPOSITION,
  SHOWRESULTSEARCH,
} from '../constant';

const initialState = {
  lastPosition: [51.505, -0.09],
  showResultSearch: false,
};

export default (state = initialState, action: ActionsModel) => {
  switch (action.type) {
    case SETLASTPOSITION:
      return { ...state, lastPosition: action.payload }
    case SHOWRESULTSEARCH:
      return { ...state, showResultSearch: action.payload };
    default:
      return state;
  }
};