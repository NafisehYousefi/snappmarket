import ActionsModel from './reducersModel/actionModel';
import {
  SETALLPOSITION,
  SHOWMODAL,
  SHOWEDITMODAL,
} from '../constant';

const initialState = {
  markers: [{
    MarkerID: '51.509,-0.11',
    Title: 'Here',
    Description: 'Anything',
    Position: [51.509, -0.11],
  }, {
    MarkerID: '51.512,-0.08',
    Title: 'Here 2',
    Description: 'Anything',
    Position: [51.512, -0.08],
  }],
  showModal: false,
  showEditModal: null,
};

export default (state = initialState, action: ActionsModel) => {
  switch (action.type) {
    case SETALLPOSITION:
      return { ...state, markers: action.payload }
    case SHOWMODAL:
      return { ...state, showModal: action.payload };
    case SHOWEDITMODAL:
      return { ...state, showEditModal: action.payload };
    default:
      return state;
  }
};