declare namespace Reducer {

  export interface CombinedReducer{
    appReducer: AppReducer,
    mapReducer: MapReducer,
    markersReducer: MarkersReducer,
    modalReducer: ModalReducer,
    searchReducer: SearchReducer
  }

  export interface AppReducer {
    showMap: boolean,
    showSplash: boolean
  }

  export interface MapReducer {
    showModal: boolean
  }

  export interface MarkersReducer {
    lastPosition: any,
    showEditModal: MarkerModel.Marker,
    showModal: boolean,
  }

  export interface ModalReducer {
    markers: MarkerModel.Marker[],
    showEditModal: MarkerModel.Marker,
    showModal: boolean,
  }

  export interface SearchReducer {
    lastPosition: any,
    showResultSearch: boolean,
  }

}