import ActionsModel from './reducersModel/actionModel';
import {
  SHOWMODAL,
} from '../constant';

const initialState = {
  showModal: false,
};

export default (state = initialState, action: ActionsModel) => {
  switch (action.type) {
    case SHOWMODAL:
      return { ...state, showModal: action.payload }
    default:
      return state;
  }
};