import ActionsModel from './reducersModel/actionModel';
import {
  SHOWSPLASH,
  SHOWMAP
} from '../constant';

const initialState = {
  showSplash: false,
  showMap: false,
};

export default (state = initialState, action: ActionsModel) => {
  switch (action.type) {
    case SHOWSPLASH:
      return { ...state, showSplash: true }
    case SHOWMAP:
      return { ...state, showMap: true }
    default:
      return state;
  }
};