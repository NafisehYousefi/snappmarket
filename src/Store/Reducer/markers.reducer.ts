import ActionsModel from './reducersModel/actionModel';
import {
  SETLASTPOSITION,
  SHOWEDITMODAL,
  SHOWMODAL,
} from '../constant';

const initialState = {
  lastPosition: [51.505, -0.09],
  showEditModal: null,
  showModal: false,
};

export default (state = initialState, action: ActionsModel) => {
  switch (action.type) {
    case SETLASTPOSITION:
      return { ...state, lastPosition: action.payload }
    case SHOWEDITMODAL:
      return { ...state, showEditModal: action.payload }
    case SHOWMODAL:
      return { ...state, showModal: action.payload }
    default:
      return state;
  }
};